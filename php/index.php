<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css">
    <script src="js/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            // Prevent scroll on # click
            $('a[href="#"]').click(function (e) {
                e.preventDefault();
            });

        });
    </script>
</head>

<body>
    <div class="landing">
        <div class="landing-title">WalletShare</div>
        <div class="landing-description">Paying for you friends has never been so easy !</div>
        <a href="wallet/index.php?action=new" class="landing-button">Create a Wallet</a>
        <?php if(isset($_SESSION['isLoggedIn'])): ?>
        <a href="wallet/index.php?action=account" class="landing-button">Open Saved Wallets</a>
        <?php else: ?>
        <a href="wallet/index.php?action=login" class="landing-button">Login</a>
        <?php endif; ?>
    </div>
    <div class="wrapper">
        <div class="page-header">
            What is WalletShare ?
        </div>
        <div class="page-content">
            WalletShare is a tool for managing group expenses. It tracks any spending and how much everyone owes or is owed !
            <br>No registration needed. Simply create a Wallet and share the link with your group.
            <br>You will be able to add every participant when you create the Wallet. Anyone joining the Wallet will have to choose its identity.
        </div>
    </div>
    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>