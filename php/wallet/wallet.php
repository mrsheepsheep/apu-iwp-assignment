<?php

    if (!isset($_GET['wallet'])){
        header('Location: ../index.php');
        exit();
    }
    $wallet_id = $_GET['wallet'];
    try {
        $wallet = get_wallet($pdo, $wallet_id);
        if ($wallet == null){
            header('Location: ../index.php');
            exit();
        }
        // We can start storing session variables
        if ((isset($_SESSION['wallet_id']) && $_SESSION['wallet_id'] != $wallet_id )|| !isset($_SESSION['participant'])){
            header("Location: index.php?wallet=$wallet_id&action=participant");
            exit();
        }
        
        // Save wallet if logged in
        if (isset($_SESSION['email']) && isset($_GET['save'])){
            save_wallet($pdo, $_SESSION['email'], $wallet_id);
            header('Location: index.php?action=account&saved');
        }

        // At this point there should be no errors fetching other data
        $participants = get_participants($pdo, $wallet_id);
        $expenses = get_expenses($pdo, $wallet_id);
        if ($expenses != null){
            $latest_expense = array_pop(array_reverse($expenses))[0];
        }
        $balance_updates = get_balance_updates($pdo, $wallet_id);

        // Generate total balance array
        $balance_total = [];
        $balance_total_abs = [];
        foreach($participants as $id => $name){
            $balance_total[$id] = 0;
            $balance_total_abs[$id] = 0;
        }
        // Substract for each "for whom"
        if ($balance_updates != null){
            foreach($balance_updates as $expense_id => $updates){
                foreach($updates as $update){
                    $balance_total[$update['participant']] -= $update['balance'];
                }
            }
            // Add for each "paid by"
            foreach($expenses as $expense){
                $balance_total[$expense[0]['participant']] += $expense[0]['total'];
            }

            // Also create an absolute total balance, to determine the abs max amount
            foreach($balance_total as $id => $balance){
                $balance_total_abs[$id] = abs($balance);
            }
        }

        // Calculate "who owes to who"
        $total = $balance_total_abs;
        $pos = [];
        $neg = [];
        foreach($balance_total as $i => $bal){
            if ($bal > 0){
                array_push($pos, $i);
            } elseif ($bal < 0){
                array_push($neg, $i);
            }
        }
        sort($pos);
        sort($neg);
        $owe_operations = [];
        foreach($neg as $ni){
            foreach($pos as $pi){
                if ($total[$ni] > $total[$pi]){
                    $diff = $total[$pi];
                } else {
                    $diff = $total[$ni];
                }
                if ($diff > 0){
                    $total[$ni] -= $diff;
                    $total[$pi] -= $diff;

                    array_push($owe_operations, [$ni, $pi, $diff]);
                }
            }
        }
        
    } catch (PDOException $e){
        header('Location: ../index.php');
        exit($e->getMessage());
    }
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $wallet['name']; ?> - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <script src="../js/jquery.min.js"></script>
    <?php if($wallet['background_image'] != null): $bg = base64_encode($wallet['background_image']); ?>
    <style>
    body {
        background-image: url("data:image/png;base64,<?= $bg ?>");
        background-position: center;
        background-size: cover;
        background-attachment: fixed;
    }
    </style>
    <?php endif; ?>
    <script>
    $(document).ready(function() {
        // Prevent scroll on # click
        $('a[href="#"]').click(function(e) {
            e.preventDefault();
        });
        // Expense item sliding
        $(".expense-item-details-toggle").each(function() {
            var details = $(this).prev(".expense-item-details");
            var link = $(this);
            link.click(function() {
                details.slideToggle(200,
                    function() {
                        if (details.is(':visible')) {
                            link.text('Less details');
                        } else {
                            link.text('More details');
                        }
                    });
            });
        });
        // Balance bar animation
        $('.balance-bar-bar').each(function() {
            var width = $(this).data('width');
            $(this).animate({
                'width': width + '%'
            }, 500);
        });

        $(".delete-expense").click(function() {
            if (confirm('Are you sure you want to delete this expense ?')) {
                var expense_id = $(this).data('expense');
                window.location.href =
                    "index.php?wallet=<?= $wallet_id; ?>&action=expense&delete&expense=" + expense_id;
            }
        });
    });
    </script>
</head>

<body>
    <div class="wrapper">
        <div class="page-header">
            <?= $wallet['name']; ?>
            <?php if($expenses != null): ?>
            <small>Last update: <?= $latest_expense['edited']; ?> by
                <?= $participants[$latest_expense['editor']]; ?>
                <?= is_self($latest_expense['editor']); ?></small>
            <?php endif; ?>
        </div>
        <div class="page-content">
            <div class="grid-container">
                <div class="expenses">
                    <?php if($expenses == null): ?>
                    <p>Nothing yet ! Add a new expense and it will appear here.</p>
                    <?php else:
                        // Display each expense here. Calculate global balance at the same time !
                        foreach(array_reverse($balance_updates) as $expense_id => $updates):
                            // Find corresponding expense
                            $exp = $expenses[$expense_id][0];
                            // Calculate total amount paid and self amount paid
                            $total = 0;
                            $paid = 0;
                            foreach($updates as $update):
                                $total += $update['balance'];
                                if ($update['participant'] == $_SESSION['participant']):
                                    $paid = $update['balance'];
                                endif;
                            endforeach;
                            // Self amount paid
                            if ($exp['participant'] == $_SESSION['participant']){
                                $paid = $total - $paid;
                            } else {
                                $paid = -$paid;
                            }
                        ?>
                    <div class="expense-item">
                        <h2 class="expense-item-title"><?= $exp['title']; ?></h2>
                        <small class="expense-item-small">$<?= $total; ?> paid by
                            <b>
                                <?= $participants[$exp['participant']]; ?>
                                <?= is_self($exp['participant']); ?>
                            </b>
                        </small>
                        <div class="expense-item-balance-impact <?= $paid >= 0 ? "positive" : "negative"; ?>">
                            <?= $paid >= 0 ? "+" : "-"; ?>$<?= abs($paid); ?></div>
                        <div class="expense-item-details hidden">
                            <p>
                                <?= $exp['detail']; ?>
                            </p>
                            <?php if(isset($exp['image'])): ?>
                            <img src="data:image/png;base64,<?= base64_encode($exp['image']); ?>">
                            <?php endif; ?>
                            <div class="expense-item-data">
                                <h3>For whom</h3>
                                <?php
                                foreach($participants as $participant_id => $participant_name):
                                $balance = 0;
                                foreach($updates as $update):
                                if ($update['participant'] == $participant_id):
                                    $balance = $update['balance'];
                                    break;
                                endif;
                                endforeach;
                                ?>
                                <p class="<?= $balance == 0 ? "no-balance" : "" ?>">$<?= $balance ?>
                                    <?= $_SESSION['participant'] == $participant_id
                                    ? '<b>'.$participant_name.' '.is_self($participant_id).'</b>'
                                    : $participant_name;
                                    ?>
                                </p>
                                <?php
                                endforeach;
                                ?>
                            </div>
                            <a href="#" data-expense="<?= $expense_id ?>"
                                class="delete-expense expense-item-detail-edit">Delete</a>
                            <a href="index.php?wallet=<?= $wallet_id; ?>&action=expense&expense=<?= $expense_id; ?>"
                                class="expense-item-detail-edit">Edit</a>
                        </div>
                        <a href="#" class="expense-item-details-toggle">More details</a>
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="my-wallet">
                    <div class="sticky">
                        <div class="wallet-balance">
                            <div class="section">
                                <div class="section-title">Balance</div>
                                <?php foreach($participants as $id => $name): ?>
                                <div class="balance-item">
                                    <div class="balance-bar">
                                        <?php if ($balance_total[$id] < 0): ?>
                                        <div class="balance-bar-negative">
                                            <div class="balance-bar-text">-$<?= $balance_total_abs[$id] ?></div>
                                            <div class="balance-bar-bar"
                                                data-width="<?= $balance_total_abs[$id]/max($balance_total_abs)*100 ?>">
                                            </div>
                                        </div>
                                        <div class="balance-bar-positive">
                                            <div class="balance-bar-text"><?= $name; ?></div>
                                            <div class="balance-bar-bar" data-width="0"></div>
                                        </div>
                                        <?php else: ?>
                                        <div class="balance-bar-negative">
                                            <div class="balance-bar-text"><?= $name; ?></div>
                                            <div class="balance-bar-bar" data-width="0"></div>
                                        </div>
                                        <div class="balance-bar-positive">
                                            <div class="balance-bar-text">+$<?= $balance_total_abs[$id] ?></div>
                                            <div class="balance-bar-bar"
                                                data-width="<?= $balance_total_abs[$id]/max($balance_total_abs)*100 ?>">
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <a href="index.php?wallet=<?= $wallet_id; ?>&action=expense" class="btn expense-button">New
                            expense</a>
                        <a href="index.php?wallet=<?= $wallet_id; ?>&action=participant"
                            class="btn wrong-participant-button">Click here if you're not
                            <?= $participants[$_SESSION['participant']]; ?></a>
                        <?php if(isset($_SESSION['email'])): $saved_wallets = get_saved_wallets($pdo, $_SESSION['email']); ?>
                        <?php if($saved_wallets != null && array_key_exists($wallet_id, $saved_wallets)): ?>
                        <a href="index.php?action=account" class="btn wrong-participant-button">Open saved wallets</a>
                        <?php else: ?>
                        <a href="index.php?action=wallet&wallet=<?= $wallet_id; ?>&save"
                            class="btn wrong-participant-button">Save</a>
                        <?php endif; ?>
                        <?php else: ?>
                        <a href="index.php?action=login" class="btn expense-button">Login to save this wallet</a>
                        <?php endif; ?>
                        <?php if(sizeof($owe_operations) > 0): ?>
                        <div class="section">
                            <div class="section-title">Who owes to who ?</div>
                            <ul class="owe-list">
                                <?php foreach($owe_operations as $operation): ?>
                                <li class="<?= $operation[0] == $_SESSION['participant'] || $operation[1] == $_SESSION['participant'] ? '' : 'ignore' ?>">
                                    <b><?= $participants[$operation[0]]; ?> <?= is_self($operation[0]); ?></b> owes <b><?= $participants[$operation[1]]; ?> <?= is_self($operation[1]); ?></b>
                                    <span>$<?= $operation[2]; ?></span>
                                </li>
                                <?php endforeach; ?>

                            </ul>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>