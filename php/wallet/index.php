<?php
// Debug only
//ini_set('display_errors', 1);

// Database functions
/* Tutorial from https://phpdelusions.net/pdo */
$DB_HOST='127.0.0.1';
$DB_PORT = '3306';
$DB_USER = 'iwp_walletshare';
$DB_PASSWORD = 'ih2dZyHS0RC1Xv5G';
$DB_DATABASE = 'iwp_walletshare';

$dsn = "mysql:host=$DB_HOST;dbname=$DB_DATABASE;charset=utf8mb4";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $DB_USER, $DB_PASSWORD, $options);
} catch (PDOException $e){
    exit('Database error. ' . $e->getMessage());
}
$actions = [
    "new" => "new.php",
    "expense" => "expense.php",
    "participant" => "participant.php",
    "account" => "account/index.php",
    "login" => "account/login.php",
    "register" => "account/register.php",
    "admin" => "account/admin.php"
];
session_start();
if (isset($_GET['action'])){
    if (array_key_exists($_GET['action'], $actions)){
        require_once($actions[$_GET['action']]);
    } else {
        require_once('wallet.php');
    }
} else {
    require_once('wallet.php');
}

/**
 * Returns wallet row from database. Null if doesn't exist.
 */
function get_wallet($pdo, $wallet_id){
    $wallet = null;
    $sql = "SELECT * FROM ws_wallet WHERE uid = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id]);
    if ($stmt->rowCount() > 0){
        $wallet = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    return $wallet;
}

/**
 * Attempts to create a new wallet in the database. Returns 0 if 
 */
function add_wallet($pdo, $title, $participants, $background_image){
    try {
        $pdo->beginTransaction();
        // Create wallet
        $success = false;
        $sql = "INSERT INTO ws_wallet(uid, name, background_image) VALUES (?,?,?)";
        $wallet_uid = null;
        while (!$success){
            $wallet_uid = uniqid();
            try {
                $pdo->prepare($sql)->execute([$wallet_uid, $title, $background_image]);
                $success = true;
            } catch (PDOException $e){
                if ($e->errorInfo[1] == 1062){
                    // Do nothing
                    //exit($e->getMessage());
                    //$pdo->rollback();
                } else {
                    throw $e;
                }
            }
        }

        $sql = "INSERT INTO ws_participants VALUES (?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        foreach($participants as $id => $name){
            $stmt->execute([$wallet_uid, $id, htmlentities($name)]);
        }
        $pdo->commit();
    } catch (PDOException $e){
        $pdo->rollback();
        exit($e);
    }
    //$pdo->commit();
    return $wallet_uid;
}

/**
 * Returns all participants from a wallet
 */
function get_participants($pdo, $wallet_id){
    $participants = null;
    $sql = "SELECT id, name FROM ws_participants WHERE wallet_uid = ? ORDER BY id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id]);
    if ($stmt->rowCount() > 0){
        $result = $stmt->fetchAll();
    }

    $participants = array();
    foreach($result as $participant){
        $participants[$participant['id']] = $participant['name'];
    }
     
    //print_r($participants);
    //exit();

    return $participants;
}

/**
 * Returns true if participant exists in wallet
 */
function participant_exists($id, $participants){
    foreach($participants as $participant){
        $participant = $participant[0];
        if ($participant['id'] == $id){
            return true;
        }
    }
    return false;
}

/**
 * Returns expenses without balance details
 */
function get_expenses($pdo, $wallet_id){
    $expenses = null;
    $sql = "SELECT * FROM ws_expenses WHERE wallet_uid = ? ORDER BY created DESC";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id]);
    if ($stmt->rowCount() > 0){
        $expenses = $stmt->fetchAll(PDO::FETCH_GROUP);
    }

    return $expenses;
}

/**
 * Returns a single expense
 */
function get_expense($pdo, $wallet_id, $expense_id){
    $expense = null;
    $sql = "SELECT participant, title, detail, image, total FROM ws_expenses WHERE wallet_uid = ? AND expense_uid = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id, $expense_id]);
    if ($stmt->rowCount() > 0){
        $expense = $stmt->fetch();
    }
    return $expense;
}

/**
 * Creates a new expense for specific wallet
 * $updates is an array containing each participant balance change
 * [
 *  participant_id => balance
 * ]
 */
function new_expense($pdo, $wallet_id, $participant, $title, $detail, $image, $total, $creator, $updates){
    $pdo->beginTransaction();
    // Insert expense
    $success = false;
    $sql = "INSERT INTO ws_expenses (expense_uid, wallet_uid, participant, title, detail, image, total, creator, editor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    while (!$success){
        $expense_uid = uniqid();
        try {
            $pdo->prepare($sql)->execute([$expense_uid, $wallet_id, $participant, $title, $detail, $image, $total, $participant, $participant]);
            $success = true;
        } catch (PDOException $e){
            if ($e->errorInfo[1] == 1062){
                $pdo->rollback();
            } else {
                throw $e;
            }
            exit($e->getMessage());
        }
    }

    // Insert balance changes
    try {
        $sql = "INSERT INTO ws_updates VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        foreach($updates as $participant_id => $balance){
            $stmt->execute([$expense_uid, $wallet_id, $participant_id, $balance]);
        }
    } catch (PDOException $e){
        $pdo->rollback();
        exit($e->getMessage());
    }
    $pdo->commit();
    return $expense_uid;
}

/**
 * Edit both expense and balance updates in the database
 */
function edit_expense($pdo, $wallet_id, $expense_id, $participant, $title, $detail, $image, $total, $editor, $updates){
    $pdo->beginTransaction();
    try {
        $sql = "UPDATE ws_expenses SET participant = ?, title = ?, detail = ?, image = ?, total = ?, editor = ?, edited = now() WHERE wallet_uid = ? AND expense_uid = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$participant, $title, $detail, $image, $total, $editor, $wallet_id, $expense_id]);

        // Delete all previous balance updates before reinserting new ones
        $sql = "DELETE FROM ws_updates WHERE wallet_uid = ? AND expense_uid = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$wallet_id, $expense_id]);

        // Insert new updates
        $sql = "INSERT INTO ws_updates VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        foreach($updates as $participant_id => $balance){
            $stmt->execute([$expense_id, $wallet_id, $participant_id, $balance]);
        }
    } catch (PDOException $e){
        if ($e->errorInfo[1] == 1062){
            $pdo->rollback();
        } else {
            throw $e;
        }
        exit($e->getMessage());
    }
    $pdo->commit();
    return true;
}

/**
 * Delete expense
 */
function delete_expense($pdo, $wallet_id, $expense_id){
    // Delete expense
    $sql = "DELETE FROM ws_expenses WHERE wallet_uid = ? AND expense_uid = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id, $expense_id]);

    // Delete all associated balance updates
    $sql = "DELETE FROM ws_updates WHERE wallet_uid = ? AND expense_uid = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id, $expense_id]);
}

/**
 * Returns all balance changes from given wallet
 */
function get_balance_updates($pdo, $wallet_id){
    $updates = null;
    $sql = "SELECT * FROM ws_updates WHERE wallet_uid = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id]);
    if ($stmt->rowCount() > 0){
        $updates = $stmt->fetchAll(PDO::FETCH_GROUP);
    }
    return $updates;
}

/**
 * Returns balance changes from specific transaction of given wallet
 */
function get_balance_update($pdo, $wallet_id, $expense_uid){
    $updates = null;
    $sql = "SELECT participant, balance FROM ws_updates WHERE wallet_uid = ? AND expense_uid = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id, $expense_uid]);
    if ($stmt->rowCount() > 0){
        $updates = $stmt->fetchAll(PDO::FETCH_GROUP);
    }
    return $updates;
}

/**
 * Returns an array of people that owes to someone, and how much
 */
function get_owes($pdo, $wallet_id){
    $owes = null;
    $sql = "SELECT ws_expenses.participant, ws_updates.participant, SUM(ws_updates.balance) AS total FROM ws_expenses JOIN ws_updates WHERE ws_expenses.participant = 1 AND ws_updates.participant != 1 AND ws_updates.wallet_uid = ? AND ws_expenses.wallet_uid = ? GROUP BY ws_updates.participant";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$wallet_id, $wallet_id]);
    if ($stmt->rowCount() > 0){
        $owes = $stmt->fetchAll(PDO::FETCH_GROUP);
    };
    return $owes;
}

/**
 * Creates a new account.
 */
function new_account($pdo, $email, $password){
    try {
        // Hash the password
        $password = hash("sha256", $password);
        $sql = "INSERT INTO ws_users VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$email, $password]);
        return true;
    } catch (PDOException $e){
        if ($e->errorInfo[1] == 1062){
            return false;
        } else {
            exit($e->getMessage());
        }
    }
}

/**
 * Checks email and password of a user
 */
function login($pdo, $email, $password){
    try {
        $password = hash("sha256", $password);
        $sql = "SELECT email FROM ws_users WHERE email = ? AND password = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$email, $password]);
        if ($stmt->rowCount() == 1){
            return true;
        }
        return false;
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}

/**
 * Retrieves saved wallets ids of specified email
 */
function get_saved_wallets($pdo, $email){
    try {
        $wallets = null;
        $sql = "SELECT wallet_uid FROM ws_saved_wallets WHERE email = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$email]);
        if ($stmt->rowCount() > 0){
            $wallets = $stmt->fetchAll(PDO::FETCH_GROUP);
        }
        return $wallets;
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}

/**
 * Deletes a wallet from account
 */
function delete_saved_wallet($pdo, $email, $wallet_id){
    try {
        $sql = "DELETE FROM ws_saved_wallets WHERE email = ? AND wallet_uid = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$email, $wallet_id]);
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}

/**
 * Saves a wallet
 */
function save_wallet($pdo, $email, $wallet_id){
    try {
        $sql = "INSERT INTO ws_saved_wallets VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$email, $wallet_id]);
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}

function change_password($pdo, $email, $password){
    try {
        $password = hash("sha256", $password);
        $sql = "UPDATE ws_users SET password = ? WHERE email = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$password, $email]);
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}

/**
 * Admin function to retrieve all wallets.
 */
function get_all_wallets($pdo, $limit = 50, $offset = 0){
    try {
        $sql = "SELECT * FROM ws_wallet LIMIT ? OFFSET ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$limit, $offset]);
        return $stmt->fetchAll(PDO::FETCH_GROUP);
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}

/**
 * Admin function to delete a wallet.
 */
function delete_wallet($pdo, $wallet_id){
    try {
        $sql = "DELETE FROM ws_wallet WHERE uid = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$wallet_id]);
        return true;
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}
/**
 * Returns '(You)' if participant ID is the same as defined in the session
 */
function is_self($participant_id){
    return $participant_id == $_SESSION['participant'] ? "(You)" : "";
}

/**
 * Utility method to quickly debug variables
 */
function debug($var){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}