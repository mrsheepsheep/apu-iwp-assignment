<?php
if (isset($_GET['wallet'])){
    $wallet_id = $_GET['wallet'];
    try {
        $wallet = get_wallet($pdo, $wallet_id);
        if ($wallet == null){
            header('Location: ../index.php');
            exit();
        }
        $participants = get_participants($pdo, $wallet_id);

        // This variable should last as long as the user has not changed wallet
        $_SESSION['wallet_id'] = $wallet_id;
    } catch (PDOException $e){
        exit($e->getMessage());
    }
}

if (isset($_POST['participant']) && isset($_SESSION['wallet_id']) && array_key_exists($_POST['participant'], $participants)){
    /*
    Setting this session variable allows us to detect when the
    current user changes the wallet, and therefore has to change his participant id
    */
    $_SESSION['participant'] = $_POST['participant'];
    header("Location: index.php?wallet=$wallet_id");
} else if (!isset($_GET['wallet'])){
    header('Location: ../index.php');
    exit();
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Who are you ? - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <?php if($wallet['background_image'] != null): $bg = base64_encode($wallet['background_image']); ?>
    <style>
        body {
            background-image: url("data:image/png;base64,<?= $bg ?>");
            background-position: center;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>
    <?php endif; ?>
</head>

<body>
    <div class="wrapper mini">
        <div class="page-header">
            <?= $wallet['name']; ?> - Who are you ?
        </div>
        <div class="page-content">
            <form action="index.php?wallet=<?= $wallet_id; ?>&action=participant" method="post">
                <label>Name</label>
                <select name="participant" required>
                    <?php foreach($participants as $id => $name): ?>
                    <option value="<?= $id; ?>"><?= $name; ?></option>
                    <?php endforeach; ?>
                </select>
                <button type="submit">Submit</button>
            </form>
        </div>
    </div>
    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>