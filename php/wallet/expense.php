<?php
if (!isset($_SESSION['wallet_id'])){
    header('Location: ../index.php');
    exit();
}

$wallet_id = $_SESSION['wallet_id'];
$action = "New";
try {
    $wallet = get_wallet($pdo, $wallet_id);
    if ($wallet == null){
        header('Location: ../index.php');
        exit();
    }
    $participants = get_participants($pdo, $wallet_id);
} catch (PDOException $e){
    exit($e->getMessage());
}

// Edit mode
if(isset($_GET['expense'])){
    $expense_id = $_GET['expense'];
    if (isset($_GET['delete'])){
        delete_expense($pdo, $wallet_id, $expense_id);
        header("Location: index.php?wallet=$wallet_id");
        exit();
    } else {
        $action = "Edit";
        $expense = get_expense($pdo, $wallet_id, $expense_id);
        if ($expense == null){
            header("Location: index.php?wallet=$wallet_id");
            exit();
        }
        $balance_updates = get_balance_update($pdo, $wallet_id, $expense_id);
    }
}

if(isset($_POST['title']) && isset($_POST['paid_by']) && isset($_POST['participants'])){
    $total = 0.0;
    $updates = [];
    if (array_key_exists(intval($_POST['paid_by']), $participants)){
        foreach($_POST['participants'] as $participant_id => $info){
            if (isset($participants[$participant_id]) && isset($info['checked']) && floatval($info['amount']) > 0){
                $updates[$participant_id] = floatval($info['amount']);
                $total += floatval($info['amount']);
            }
        }
        if ($total != 0){
            try {
                if (isset($_SESSION['participant'])){
                    $image = null;
                    if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != ""){
                        // Check image is actually an image
                        $valid = getimagesize($_FILES['image']["tmp_name"]);
                        if ($valid !== false){
                            $image = file_get_contents($_FILES['image']['tmp_name']);
                        }
                    }
                    if (isset($_GET['expense'])){
                        edit_expense($pdo, $wallet_id, $expense_id, intval($_POST['paid_by']), htmlentities($_POST['title']), htmlentities($_POST['detail']), $image, floatval($total), $_SESSION['participant'], $updates);
                    } else {
                        new_expense($pdo, $wallet_id, intval($_POST['paid_by']), htmlentities($_POST['title']), htmlentities($_POST['detail']), $image, floatval($total), $_SESSION['participant'], $updates);
                    }
                    header("Location: index.php?wallet=$wallet_id");
                } else {
                    header("Location: index.php?wallet=$wallet_id&action=participant");
                    exit();
                }
            } catch (PDOException $e){
                exit($e->getMessage());
            }
        } else {
            header("Location: index.php?wallet=$wallet_id&action=expense&zero");
            exit();
        }
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $action; ?> expense - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <?php if($wallet['background_image'] != null): $bg = base64_encode($wallet['background_image']); ?>
    <style>
        body {
            background-image: url("data:image/png;base64,<?= $bg ?>");
            background-position: center;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>
    <?php endif; ?>
    <script src="../js/jquery.min.js"></script>
    <script>
    $(document).ready(function() {
        $("input[type=number]").change(function() {
            if ($(this).val() < 0) {
                $(this).val(0);
            }
        });
        $("input[type=checkbox]").change(function() {
            var count = $(".for-whom-item input:checked").length;
            console.log($(this).prop('checked'));
            if ($(this).prop('checked') == 'true') {
                var total = 0;
                $("input:checked").each(function() {
                    total += Math.abs($(this).next('input[type=number]').val());
                });
                $("#total").val(total);
            } else {
                var value = $("#total").val();
                var count = $(".for-whom-item input:checked").length;
                var split = Math.round(value / count * 100) / 100;
                $(".for-whom-item input:checked").next('input[type=number]').val(split);
            }
        });
        $("#total").change(function() {
            var value = $(this).val();
            var count = $(".for-whom-item input:checked").length;
            var split = Math.round(value / count * 100) / 100;
            $(".for-whom-item input:checked").next('input[type=number]').val(split);
        });
        /*
        $(".for-whom-item input[type=number]").change(function() {
            var parent = $(this).parent();
            var id = parent.data('participant');
            var checked = parent.children('input:checked');
            var value = $(this).val();
            if (checked.length > 0) {
                recalculate(value, id);
            }
        });
        */

        $("#expense").submit(function(e){
            var total_calc = 0;
            $("input:checked").each(function() {
                total_calc += Math.abs($(this).next('input[type=number]').val());
            });
            var total = $("#total").val();
            if (total_calc != total){
                alert("Total is wrong. Please make sure the sum equals the total and try again !");
                e.preventDefault();
            }
        });

        function recalculate(value, ignoreId) {
            var count = $(".for-whom-item input:checked").length - 1;
            var total = $("#total").val() - value;
            var split = Math.round(total / count * 100) / 100;
            if (total < 0) {
                split = 0;
                $(".for-whom-item[data-participant=" + ignoreId + "] input[type=number]").val($("#total")
                    .val());
            }
            $(".for-whom-item input:checked").each(function() {
                if ($(this).parent().data('participant') != ignoreId) {
                    $(this).next('input[type=number]').val(split);
                }
            });
        }
    });
    </script>
</head>

<body>
    <div class="wrapper mini">
        <div class="page-header">
            <?= $action; ?> expense for <?= $wallet['name']; ?>
        </div>
        <div class="page-content">
            <?php if(isset($_GET['zero'])): ?>
            <div class="alert alert-error">
                <b>Error: </b> Total cannot be zero !
            </div>
            <?php endif; ?>
            <form id="expense" action="" enctype="multipart/form-data" method="post">
                <?php if(isset($expense_id)): ?>
                <input type="hidden" name="expense" value="<?= $expense_id; ?>">
                <?php endif;?>
                <label>Title</label>
                <input type="text" id="title" name="title" required value="<?= isset($expense_id) ? $expense['title'] : '' ?>">
                <label>Details</label>
                <textarea id="detail" name="detail" placeholder="Optional"><?= isset($expense_id) ? $expense['detail']: '' ?></textarea>
                <label>Add a photo</label>
                <input type="file" accept="image/png image/jpeg" capture="environment" id="image" name="image">
                <label>Paid by</label>
                <select name="paid_by" required>
                <?php foreach($participants as $id => $name): ?>
                    <option <?= ($id == $_SESSION['participant'] && !isset($_GET['expense']) || (isset($_GET['expense']) && $id == $expense['participant'])) ? 'selected' : '' ?> value="<?= $id; ?>">
                    <?= $name; ?></option>
                <?php endforeach; ?>
                </select>
                <div class="for-whom">
                    <h2>For whom</h2>
                    <small>Total must be defined first. You can play with the checkboxes to split the
                        amount.</small><br><br>
                    <?php foreach($participants as $id => $name): ?>
                    <div class="for-whom-item" data-participant="<?= $id; ?>">
                        <input <?= isset($expense) && isset($balance_updates[$id]) ? 'checked' : '' ?> type="checkbox" name="participants[<?= $id; ?>][checked]">
                        <input value="<?= isset($expense) && isset($balance_updates[$id]) ? floatval($balance_updates[$id][0]['balance']) : '' ?>" step="0.01" type="number" name="participants[<?= $id; ?>][amount]">
                        <label><?= $name; ?></label>
                    </div>
                    <?php endforeach; ?>
                    <h2 class="total">Total: <input value="<?= isset($expense) ? floatval($expense['total']) : '0' ?>" name="total" id="total" type="number" step="0.01"></h2>
                </div>
                <button type="submit">Submit</button><a class="btn" href="index.php?wallet=<?= $wallet_id ?>">Cancel</a>
            </form>
        </div>
    </div>
    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>