<?php

$password = "b99213b33f1452b85f0abb7b89a41e685cde120d111b29e22e5f0f0cf2ba871e";

if(isset($_POST['password'])){
    if ($password == hash("sha256", $_POST['password'])){
        $_SESSION['admin'] = true;
    }
}

if (isset($_SESSION['admin'])){
    if (isset($_POST['deletewallet'])){
        delete_wallet($pdo, $_POST['deletewallet']);
        header('Location: index.php?action=admin');
    }

    $limit = 50;
    $offset = 0;
    if (isset($_GET['search'])){
        $offset = intval($_GET['search']);
    }
    $wallet_ids = get_all_wallets($pdo, $limit, $offset);
    $wallets = [];
    if ($wallet_ids != null){
        foreach($wallet_ids as $id => $v){
            $wallets[$id] = [];
            $wallets[$id] = $v[0];
            $wallets[$id]['uid'] = $id;
            $wallets[$id]['participants'] = sizeof(get_participants($pdo, $id));
            $expenses = get_expenses($pdo, $id);
            $wallets[$id]['expenses'] = 0;
            if ($expenses != null){
                $wallets[$id]['expenses'] = sizeof($expenses);
                $wallets[$id]['last_expense'] = array_pop(array_reverse($expenses))[0];
            }
        }
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administration - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <script src="../js/jquery.min.js"></script>
    <script>
    $(document).ready(function() {
        // Prevent scroll on # click
        $('a[href="#"]').click(function(e) {
            e.preventDefault();
        });
        // Expense item sliding
        $(".expense-item-details-toggle").each(function() {
            var details = $(this).prev(".expense-item-details");
            var link = $(this);
            link.click(function() {
                details.slideToggle(200,
                    function() {
                        if (details.is(':visible')) {
                            link.text('Less details');
                        } else {
                            link.text('More details');
                        }
                    });
            });
        });

        $(".delete-wallet").click(function(e) {
            if (confirm('Are you sure you want to delete this wallet ?')) {
                var wallet_id = $(this).data('wallet');
                var form = document.createElement("form");
                form.method = "post";
                form.action = "index.php?action=admin";
                var deletewallet = document.createElement("input");
                deletewallet.name = "deletewallet";
                deletewallet.value = wallet_id;
                deletewallet.type = "text";
                deletewallet.id = "deletewallet";
                form.appendChild(deletewallet);
                $("body").append(form);
                form.submit();
            }
        });
    });
    </script>
</head>

<body>
    <?php if(!isset($_SESSION['admin'])): ?>
    <div class="wrapper mini">
        <div class="page-header">This page is password protected</div>
        <div class="page-content">
            <form method="post">
                <label>Password</label>
                <input type="password" name="password">
                <button type="submit">Login</button>
            </form>
        </div>
    </div>
    <?php else: ?>
    <div class="wrapper mini">
        <div class="page-header">
            Administration
        </div>
        <div class="page-content">
            <div class="section">
                <h3 class="section-title">Search</h3>
                <form method="get">
                    <label>Number of wallets to show
                    <input type="number" name="search">
                    <button type="submit">Search</button>
                </form>
            </div>
            <div class="section">
                <h3 class="section-title">Wallets (<?= $offset ?> to <?= ($offset + $limit); ?>)</h3>
                <?php if(isset($_GET['deletesuccess'])): ?>
                <div class="alert alert-success">The wallet was successfully deleted.</div>
                <?php endif; ?>
                <?php if(sizeof($wallets) > 0): ?>
                    <?php foreach($wallets as $wallet): ?>
                    <!-- One wallet -->
                    <div class="expense-item">
                        <h2 class="expense-item-title"><?= $wallet['name']; ?> <small
                                class="expense-item-small">#<?= $wallet['uid']; ?></small></h2>
                        <div class="expense-item-balance-impact positive"><a
                                href="index.php?action=wallet&wallet=<?= $wallet['uid']; ?>">Open</a>
                        </div>
                        <div class="expense-item-details hidden">
                            <ul>
                                <li>Participants: <?= $wallet['participants']; ?></li>
                                <li>Date of creation: <?= $wallet['created']; ?></li>
                                <li>Last update: <?= $wallet['last_expense']['edited']; ?></li>
                                <li>Expenses: <?= $wallet['expenses']; ?></li>
                            </ul>
                            <a href="#" data-wallet="<?= $wallet['uid']; ?>" class="delete-wallet expense-item-detail-edit">Delete</a>
                        </div>
                        <a href="#" class="expense-item-details-toggle">More details</a>
                    </div>
                    <!-- End of wallet -->
                    <?php endforeach; ?>
                <?php else: ?>
                <p>No wallet was found.</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>