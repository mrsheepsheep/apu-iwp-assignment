<?php
if (isset($_POST['email']) && isset($_POST['password'])){
    $success = new_account($pdo, $_POST['email'], $_POST['password']);
    if ($success){
        header('Location: index.php?action=login&registered=1');
        exit();
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <script src="../js/jquery.min.js"></script>
</head>

<body>
    <div class="wrapper mini">
        <div class="page-header">
            <h1>Register</h1><small>Create an account to save your wallets</small>
        </div>
        <div class="page-content">
            <?php if (isset($success)): ?>
                <div class="alert alert-error">
                    <b>Error:</b> This email already exists.
                </div>
            <?php endif; ?>
            <form action="index.php?action=register" method="post">
                <label>Email address</label>
                <input type="email" name="email" required>
                <label>Password</label>
                <input type="password" name="password" required>
                <button type="submit" value="Create Wallet">Register</button>
            </form>
        </div>
    </div>
    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>