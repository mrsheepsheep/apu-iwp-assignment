<?php
if (!(isset($_SESSION['loggedIn']) && isset($_SESSION['email']))){
    header('Location: index.php?action=login');
    exit();
} elseif(isset($_GET['logout'])){
    unset($_SESSION['loggedIn']);
    unset($_SESSION['email']);
    header('Location: ../index.php');
    exit();
}

if (isset($_GET['deletewallet'])){
    delete_saved_wallet($pdo, $_SESSION['email'], $_GET['deletewallet']);
    header('Location: index.php?action=account&deletesuccess');
}
if (isset($_POST['password']) && isset($_POST['confirm'])){
    $password = $_POST['password'];
    $confirm = $_POST['confirm'];
    if ($password == $confirm){
        change_password($pdo, $_SESSION['email'], $password);
        header('Location: index.php?action=account&passwordchanged=true');
    } else {
        header('Location: index.php?action=account&passwordchanged=false');
    }
}

$wallet_ids = get_saved_wallets($pdo, $_SESSION['email']);
$wallets = [];
if ($wallet_ids != null){
    foreach($wallet_ids as $id => $v){
        $wallets[$id] = [];
        $wallets[$id] = get_wallet($pdo, $id);
        $wallets[$id]['participants'] = sizeof(get_participants($pdo, $id));
        $expenses = get_expenses($pdo, $id);
        $wallets[$id]['expenses'] = 0;
        if ($expenses != null){
            $wallets[$id]['expenses'] = sizeof($expenses);
            $wallets[$id]['last_expense'] = array_pop(array_reverse($expenses))[0];
        }
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Account - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <script src="../js/jquery.min.js"></script>
    <script>
    $(document).ready(function() {
        // Prevent scroll on # click
        $('a[href="#"]').click(function(e) {
            e.preventDefault();
        });
        // Expense item sliding
        $(".expense-item-details-toggle").each(function() {
            var details = $(this).prev(".expense-item-details");
            var link = $(this);
            link.click(function() {
                details.slideToggle(200,
                    function() {
                        if (details.is(':visible')) {
                            link.text('Less details');
                        } else {
                            link.text('More details');
                        }
                    });
            });
        });

        $(".delete-wallet").click(function() {
            if (confirm('Are you sure you want to remove this wallet from your account ?')) {
                var wallet_id = $(this).data('wallet');
                window.location.href = "index.php?action=account&deletewallet=" + wallet_id;
            }
        });
    });
    </script>
</head>

<body>
    <div class="wrapper">
        <div class="page-header">
            Account
        </div>
        <div class="page-content">
            <div class="grid-container">
                <div class="expense">
                    <div class="section">
                        <h3 class="section-title">Saved Wallets</h3>
                        <?php if(isset($_GET['deletesuccess'])): ?>
                        <div class="alert alert-success">The wallet was successfully removed from your saved wallets.
                        </div>
                        <?php elseif(isset($_GET['saved'])): ?>
                        <div class="alert alert-success">The wallet is now saved on your account. You can open it from
                            here.</div>
                        <?php endif; ?>
                        <?php if(sizeof($wallets) > 0): ?>
                        <?php foreach($wallets as $wallet): ?>
                        <!-- One wallet -->
                        <div class="expense-item">
                            <h2 class="expense-item-title"><?= $wallet['name']; ?> <small
                                    class="expense-item-small">#<?= $wallet['uid']; ?></small></h2>
                            <div class="expense-item-balance-impact positive"><a
                                    href="index.php?action=wallet&wallet=<?= $wallet['uid']; ?>">Open</a>
                            </div>
                            <div class="expense-item-details hidden">
                                <ul>
                                    <li>Participants: <?= $wallet['participants']; ?></li>
                                    <li>Date of creation: <?= $wallet['created']; ?></li>
                                    <li>Last update: <?= $wallet['last_expense']['edited']; ?></li>
                                    <li>Expenses: <?= $wallet['expenses']; ?></li>
                                </ul>
                                <a href="#" data-wallet="<?= $wallet['uid']; ?>"
                                    class="delete-wallet expense-item-detail-edit">Remove</a>
                            </div>
                            <a href="#" class="expense-item-details-toggle">More details</a>
                        </div>
                        <!-- End of wallet -->
                        <?php endforeach; ?>
                        <?php else: ?>
                        <p>You don't have any saved wallets yet.</p>
                        <p>Add one by opening a wallet and clicking the "Save" button !</p>
                        </p>You can also <a href="index.php?action=new">create a new wallet.</a></p>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="my-wallet">
                    <div class="section">
                        <h3 class="section-title">Account information</h3>
                        <?php if(isset($_GET['passwordchanged'])): ?>
                        <?php if($_GET['passwordchanged'] == "true"): ?>
                        <div class="alert alert-success">Your password has been changed.</div>
                        <?php else: ?>
                        <div class="alert alert-error">Passwords don't match. Please try again.</div>
                        <?php endif; ?>
                        <?php endif; ?>
                        <form method="post">
                            <label>E-Mail</label>
                            <input disabled type="email" name="email" value="<?= $_SESSION['email']; ?>">
                            <label>Change password</label>
                            <input type="password" name="password" value="">
                            <label>Confirm password</label>
                            <input type="password" name="confirm" value="">
                            <button type="submit">Submit</button>
                        </form>
                    </div>
                    <a href="index.php?action=new" class="btn wrong-participant-button">New Wallet</a>
                    <a href="index.php?action=account&logout" class="btn expense-button">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>