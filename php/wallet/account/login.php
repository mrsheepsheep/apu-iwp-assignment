<?php
    if (isset($_POST['email']) && isset($_POST['password'])){
        $success = login($pdo, $_POST['email'], $_POST['password']);
        if ($success){
            $_SESSION['loggedIn'] = true;
            $_SESSION['email'] = $_POST['email'];
            header('Location: index.php?action=account');
            exit();
        }
    }
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <script src="../js/jquery.min.js"></script>
</head>

<body>
    <div class="wrapper mini">
        <div class="page-header">
            <h1>Login</h1><small>Open your saved wallets</small>
        </div>
        <div class="page-content">
            <?php if(isset($_GET['registered'])): ?>
                <div class="alert alert-success">
                    <b>Successfully registered</b><br>
                    You can now login into your account.
                </div>
            <?php elseif(isset($success)): ?>
                <?php if(!$success): ?>
                    <div class="alert alert-error">
                        <b>Error: </b> Invalid credentials.
                    </div>
                <?php endif; ?>
            <?php endif; ?> 
            <form action="index.php?action=login" enctype="multipart/form-data" method="post">
                <label>Email address</label>
                <input type="email" name="email" required>
                <label>Password</label>
                <input type="password" name="password" required>
                <button type="submit" value="Create Wallet">Login</button><a href="index.php?action=register">I don't have an account</a>

            </form>
        </div>
    </div>
    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>