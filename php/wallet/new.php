<?php
    if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['participants'])){
        $wallet_name = $_POST['name'];
        $background_image = null;
        if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != ""){
            // Check image is actually an image
            $valid = getimagesize($_FILES['image']["tmp_name"]);
            if ($valid !== false){
                $background_image = file_get_contents($_FILES['image']['tmp_name']);
            }
        }
        
        $email = $_POST['email'];
        $participants = $_POST['participants'];
        try {
            $wallet_uid = add_wallet($pdo, htmlentities($wallet_name), $participants, $background_image);
        } catch (PDOException $e){
            exit($e->getMessage());
        }
        header("Location: index.php?wallet=$wallet_uid");
        exit();
    }
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Create a new Wallet - WalletShare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css">
    <script src="../js/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            // Prevent scroll on # click
            $('a[href="#"]').click(function(e){ e.preventDefault(); });
            $('.participant-remove').click(function(){
                $(this).remove();
            });
            $('#add-participant').click(function(){
                var length = $(".participant-field").length - 1;
                if (length < 16){
                    var model = $(".participant-field:first").clone();
                    $("#participants").append(model);
                    model.show();
                    model.find('input').first().attr('name', 'participants[' + (length+1) + ']');
                    model.find('.participant-remove').click(function(){
                        model.remove();
                    });
                }
            });
        });
    </script>
</head>

<body>
    <div class="wrapper mini">
        <div class="page-header">
            <h1>Create a new Wallet</h1>
        </div>
        <div class="page-content">
            <form enctype="multipart/form-data" method="post">
                <label>Wallet Name</label>
                <input type="text" name="name" required>
                <label>E-mail address</label>
                <input type="email" name="email" <?= isset($_SESSION['email']) ? 'readonly' : '' ?> value="<?= $_SESSION['email']; ?>" required>
                <p>We will send you the link to the wallet. We do not store your email address.</p>
                <label>Participants</label>
                <div id="participants">
                    <div class="participant-field" style="display:none;">
                        <input type="text" name=""><a href="#" class="participant-remove">x</a>
                    </div>
                    <div class="participant-field">
                        <input type="text" name="participants[1]" required><a href="#" class="participant-remove">x</a>
                    </div>
                </div>
                <input value="Add participant" type="button" id="add-participant">
                <label>Background image (optional)</label>
                <input type="file" name="image" accept="image/png, image/jpeg">
                <button type="submit" value="Create Wallet">Create Wallet</button>
                <p>You will not be able to add new participants after the Wallet has been created !</p>
            </form>
        </div>
    </div>
    <footer>
        Developed by Alexandre Souleau
    </footer>
</body>

</html>